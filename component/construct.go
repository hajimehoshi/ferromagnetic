package component

type Level struct {
	Width  int
	Height int
}

func NewLevel(width, height int) Level {
	return Level{width, height}
}
