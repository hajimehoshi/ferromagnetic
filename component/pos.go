package component

import "fmt"

type Pos struct {
	X, Y, Z          float64
	OffsetA, OffsetB float64
	Grow, GrowAcc    float64
}

func NewPos[T int | float64](xyz ...T) Pos {
	ln := len(xyz)
	p := Pos{}
	if ln > 0 {
		p.X = float64(xyz[0])
	}
	if ln > 1 {
		p.Y = float64(xyz[1])
	}
	if ln > 2 {
		p.Z = float64(xyz[2])
	}
	return p
}

func (p Pos) Add(n Pos) Pos {
	return Pos{X: p.X + n.X, Y: p.Y + n.Y, Z: p.Z + n.Z}
}

func (p Pos) Sub(n Pos) Pos {
	return Pos{X: p.X - n.X, Y: p.Y - n.Y, Z: p.Z - n.Z}
}

func (p Pos) Mul(n Pos) Pos {
	return Pos{X: p.X * n.X, Y: p.Y * n.Y, Z: p.Z * n.Z}
}

func (p Pos) Div(n Pos) Pos {
	return Pos{X: p.X / n.X, Y: p.Y / n.Y, Z: p.Z / n.Z}
}

func (p Pos) Trunc() Pos {
	return Pos{X: float64(int(p.X)), Y: float64(int(p.Y)), Z: float64(int(p.Z))}
}

func (p Pos) Copy() Pos {
	return Pos{X: p.X, Y: p.Y, Z: p.Z}
}

func (p Pos) String() string {
	return fmt.Sprintf("%.2f, %.2f, %.2f", p.X, p.Y, p.Z)
}
