package component

type Size struct {
	W, H, D float64
}

func NewSize[T int | float64](xyz ...T) Size {
	ln := len(xyz)
	s := Size{}
	if ln > 0 {
		s.W = float64(xyz[0])
	}
	if ln > 1 {
		s.H = float64(xyz[1])
	}
	if ln > 2 {
		s.D = float64(xyz[2])
	}
	return s
}

func (s Size) Add(n Size) Size {
	return Size{W: s.W + n.W, H: s.H + n.H, D: s.D + n.D}
}

func (s Size) Sub(n Size) Size {
	return Size{W: s.W - n.W, H: s.H - n.H, D: s.D - n.D}
}

func (s Size) Mul(n Size) Size {
	return Size{W: s.W * n.W, H: s.H * n.H, D: s.D * n.D}
}

func (s Size) Div(n Size) Size {
	return Size{W: s.W / n.W, H: s.H / n.H, D: s.D / n.D}
}

func (s Size) Trunc() Size {
	return Size{W: float64(int(s.W)), H: float64(int(s.H)), D: float64(int(s.D))}
}

func (s Size) Copy() Size {
	return Size{W: s.W, H: s.H, D: s.D}
}
