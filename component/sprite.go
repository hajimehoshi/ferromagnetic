package component

import (
	"ferromagnetic/helper/graphics"
)

type Sprite struct {
	Frameset *graphics.Frameset
	Hue      float64
}

func NewSprite(frameset *graphics.Frameset, hue float64) Sprite {
	return Sprite{Frameset: frameset, Hue: hue}
}
