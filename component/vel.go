package component

import "fmt"

type Vel struct {
	L, M, N float64
}

func NewVel[T int | float64](xyz ...T) Vel {
	ln := len(xyz)
	v := Vel{}
	if ln > 0 {
		v.L = float64(xyz[0])
	}
	if ln > 1 {
		v.M = float64(xyz[1])
	}
	if ln > 2 {
		v.N = float64(xyz[2])
	}
	return v
}

func (v Vel) Add(n Vel) Vel {
	return Vel{L: v.L + n.L, M: v.M + n.M, N: v.N + n.N}
}

func (v Vel) Sub(n Vel) Vel {
	return Vel{L: v.L - n.L, M: v.M - n.M, N: v.N - n.N}
}

func (v Vel) Mul(n Vel) Vel {
	return Vel{L: v.L * n.L, M: v.M * n.M, N: v.N * n.N}
}

func (v Vel) Div(n Vel) Vel {
	return Vel{L: v.L / n.L, M: v.M / n.M, N: v.N / n.N}
}

func (v Vel) Trunc() Vel {
	return Vel{L: float64(int(v.L)), M: float64(int(v.M)), N: float64(int(v.N))}
}

func (v Vel) Copy() Vel {
	return Vel{L: v.L, M: v.M, N: v.N}
}

func (v Vel) String() string {
	return fmt.Sprintf("%.2f, %.2f, %.2f", v.L, v.M, v.N)
}
