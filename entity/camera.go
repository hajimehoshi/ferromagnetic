package entity

import "ferromagnetic/component"

type Camera struct {
	component.Pos
	component.Zoom
}
