package entity

import "ferromagnetic/component"

type Hole struct {
	component.Pos
	component.Radius
	component.Sprite
}
