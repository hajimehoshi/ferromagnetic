package enum

type BallState int

const (
	BallStateSeek = BallState(iota)
	BallStateMoving
)

func (b BallState) String() string {
	switch b {
	case BallStateSeek:
		return "seek"
	case BallStateMoving:
		return "moving"
	default:
		return "none"
	}
}
