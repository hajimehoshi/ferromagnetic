package graphics

import (
	"image/color"

	"golang.org/x/image/colornames"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text"
	"golang.org/x/image/font"
)

func DrawCenteredText(screen *ebiten.Image, font font.Face, s string, cx, cy int, color color.Color) {
	bounds := text.BoundString(font, s)
	x, y := cx-bounds.Min.X-bounds.Dx()/2, cy-bounds.Min.Y-bounds.Dy()/2
	text.Draw(screen, s, font, x, y, color)
}

func DrawBrightText(screen *ebiten.Image, font font.Face, s string, cx, cy int) {
	DrawCenteredText(screen, font, s, cx+1, cy+1, colornames.Black)
	DrawCenteredText(screen, font, s, cx, cy, colornames.White)
}
