package isometry

import (
	"ferromagnetic/component"
)

func FromScreen(screen component.Pos) component.Pos {
	isoX := (screen.X + screen.Y*2) / 2
	isoY := -screen.X + isoX
	return component.NewPos(isoX, isoY) //.Div(component.NewPos(2, 2))
}

func ToScreen(iso component.Pos) component.Pos {
	return component.NewPos(
		iso.X-iso.Y,
		(iso.X+iso.Y)/2,
	).Div(component.NewPos(2, 2))
}
