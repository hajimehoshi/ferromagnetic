package mat

import (
	"math"

	"ferromagnetic/component"
)

func Equal(a, b, threshold float64) bool {
	return math.Abs(a-b) < threshold
}

func Lerp(a, b, t float64) float64 {
	return a + (b-a)*t
}

func InCircleIso(circlePos component.Pos, circleRad float64, target component.Pos) bool {
	dx := math.Abs(target.X - circlePos.X)
	dy := math.Abs(target.Y - circlePos.Y)

	//square diamond inside simplified
	if dx+dy <= circleRad {
		return true
	}
	//square diamond outside simplified
	if dx > circleRad || dy > circleRad {
		return false
	}
	//remain area Pythagoras
	if dx*dx+dy*dy <= circleRad*circleRad {
		return true
	}

	return false
}

func Dist(a, b component.Pos) float64 {
	return math.Sqrt((a.X-b.X)*(a.X-b.X) + (a.Y-b.Y)*(a.Y-b.Y))
}

func VectorAngle(x, y float64) float64 {
	if x > 0 {
		if y > 0 { // 1 четверть
			x = math.Abs(x)
			y = math.Abs(y)
			return math.Atan(y / x)
		} else { // 4 четверть
			x = math.Abs(x)
			y = math.Abs(y)
			return -math.Atan(y / x)
		}
	} else {
		if y > 0 { // 2 четверть
			x = math.Abs(x)
			y = math.Abs(y)
			return math.Pi - math.Atan(y/x)
		} else { // 3 четверть
			x = math.Abs(x)
			y = math.Abs(y)
			return -math.Pi + math.Atan(y/x)
		}
	}
}

func PointsAngle(x, y, x2, y2 float64) float64 {
	x2 = x2 - x
	y2 = y2 - y
	return VectorAngle(x2, y2)
}
