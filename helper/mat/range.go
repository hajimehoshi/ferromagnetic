package mat

func Normalize(x, oldMin, oldMax, newMin, newMax float64) float64 {
	oldRange := oldMax - oldMin
	newRange := newMax - newMin
	return (((x - oldMin) * newRange) / oldRange) + newMin
}
