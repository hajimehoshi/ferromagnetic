package mat

import (
	"github.com/sedyh/mizu/pkg/engine"
)

func Index(x, y, w int) int {
	return y*w + x
}

func Call(entities []engine.Entity, x, y, w, h int, callback func(e engine.Entity)) {
	xOut := x < 0 || x >= w
	yOut := y < 0 || y >= h
	if xOut || yOut {
		return
	}

	callback(entities[Index(x, y, w)])
}
