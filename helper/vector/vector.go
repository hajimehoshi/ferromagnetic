package vector

import (
	"math"
)

func Length(x, y, z float64) float64 {
	return math.Sqrt(x*x + y*y + z*z)
}

func Normalize(x, y, z float64) (float64, float64, float64) {
	l := Length(x, y, z)
	if l == 0 {
		return 0, 0, 0
	}
	return x / l, y / l, z / l
}

func NormalizeLength(x, y, z, length float64) (float64, float64, float64) {
	if length == 0 {
		return 0, 0, 0
	}
	return x / length, y / length, z / length
}
