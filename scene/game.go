package scene

import (
	"time"

	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/helper/mat"

	"ferromagnetic/assets"
	"ferromagnetic/component"
	"ferromagnetic/entity"
	"ferromagnetic/system"
)

type Game struct{}

func (g *Game) Setup(w engine.World) {
	w.AddComponents(
		component.Pos{}, component.Vel{}, component.Sprite{}, component.Zoom{},
		component.Slide{}, component.Level{}, component.Radius{}, component.Gravity{},
		component.Death{},
	)
	w.AddSystems(
		system.NewWater(),
		system.NewTerrain(), system.NewFocus(), system.NewAnimation(),
		system.NewTilemap(), system.NewVelocity(), system.NewGravitation(), system.NewSlide(), system.NewCollision(), system.NewEscape(),
		system.NewMetaballs(), system.NewDebug(), system.NewSelection(), system.NewMerger(),
		system.NewMusic(300*time.Millisecond),
	)

	level := component.NewLevel(30, 30)

	for y := 0; y < level.Height; y++ {
		for x := 0; x < level.Width; x++ {
			w.AddEntities(&entity.Tile{
				Pos:    component.NewPos(x, y, 0),
				Sprite: component.NewSprite(assets.Images["grass"], 0),
			})
		}
	}

	for i := 0; i < 20; i++ {
		lw, lh := float64(level.Width), float64(level.Height)
		adjustX, adjustY := lw*0.4, lh*0.4
		rndX := mat.RangeF(adjustX, lw-adjustX)
		rndY := mat.RangeF(adjustY, lh-adjustY)
		w.AddEntities(&entity.Ball{
			Pos:     component.NewPos(rndX, rndY, 1),
			Vel:     component.NewVel(0, 0, 0),
			Slide:   component.NewSliding(),
			Radius:  component.NewRadius(0.5),
			Gravity: component.Gravity{Value: 0.02},
			Death:   component.Death{Is: false},
		})
	}

	w.AddEntities(
		&entity.Camera{Pos: component.NewPos(20, 20), Zoom: component.NewZoom()},
		&entity.Construct{Level: level},
	)

	w.AddEntities(&entity.Hole{
		Pos:    component.NewPos(level.Width/8, level.Height/8),
		Radius: component.NewRadius(4),
		Sprite: component.NewSprite(assets.Images["hole"], 0),
	})
}
