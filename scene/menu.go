package scene

import "github.com/sedyh/mizu/pkg/engine"

type Menu struct{}

func (m *Menu) Setup(w engine.World) {
	w.AddComponents()
	w.AddSystems()
	w.AddEntities()
}
