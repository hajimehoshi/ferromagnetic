package system

import (
	"image/color"
	"strconv"

	"ferromagnetic/assets"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/sedyh/mizu/pkg/engine"
	"golang.org/x/image/colornames"
	"golang.org/x/image/font/basicfont"

	"ferromagnetic/helper/geo"
	"ferromagnetic/helper/graphics"
)

type Debug struct{}

func NewDebug() *Debug {
	return &Debug{}
}

func (d *Debug) Draw(_ engine.World, screen *ebiten.Image) {
	sw, sh := geo.Image(screen, 1)
	//adjust := 20.
	ebitenutil.DrawRect(screen, 0, 0, sw, sh/8, color.RGBA{A: 100})
	ebitenutil.DrawRect(screen, 0, sh-sh/16, sw, sh/8, color.RGBA{A: 100})
	graphics.DrawCenteredText(screen, basicfont.Face7x13,
		"Press 'Left Mouse Key' to move the earth. Press 'Z' to attract liquid. Press 'S' to jump.",
		int(sw/2), int(sh/16-sh/64),
		colornames.White,
	)
	graphics.DrawCenteredText(screen, basicfont.Face7x13,
		"Press 'Scroll' to move the camera. Move all ferromagnetic liquid to the hole",
		int(sw/2), int(sh/16+sh/64),
		colornames.White,
	)
	graphics.DrawCenteredText(screen, basicfont.Face7x13,
		"Demo by sedyh and yamelyat. Music by sedyh. Total score: "+strconv.Itoa(assets.Counter),
		int(sw/2), int(sh-sh/16+sh/32),
		colornames.White,
	)
}
