package system

import (
	"math"

	"github.com/hajimehoshi/ebiten/v2/inpututil"

	"github.com/hajimehoshi/ebiten/v2"

	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/helper/mat"

	"ferromagnetic/assets"

	"ferromagnetic/component"
	"ferromagnetic/helper/vector"
)

type Escape struct {
	candidates []engine.Entity
}

func NewEscape() *Escape {
	return &Escape{}
}

func (e *Escape) Update(w engine.World) {
	e.candidates = e.candidates[:0]
	construct, ok := w.View(component.Level{}).Get()
	if !ok {
		return
	}
	var level *component.Level
	construct.Get(&level)

	escape, ok := w.View(component.Pos{}, component.Radius{}, component.Sprite{}).Get()
	if !ok {
		return
	}

	w.View(component.Pos{}, component.Vel{}, component.Radius{}, component.Slide{}).Each(func(eng engine.Entity) {
		e.candidates = append(e.candidates, eng)
	})

	var ePos *component.Pos
	var eRad *component.Radius
	escape.Get(&ePos, &eRad)

	for _, ball := range e.candidates {
		var bPos *component.Pos
		var bRad *component.Radius
		var bVel *component.Vel
		var bDeath *component.Death
		ball.Get(&bPos, &bRad, &bVel, &bDeath)

		distX, distY, distZ := ePos.X-bPos.X, ePos.Y-bPos.Y, ePos.Z-bPos.Z

		dist := vector.Length(distX, distY, distZ)

		if dist < eRad.Value+bRad.Value {
			dirX, dirY, dirZ := vector.NormalizeLength(distX, distY, distZ, dist)

			bVel.L += dirX * 0.5
			bVel.M += dirY * 0.5
			bVel.N += dirZ * 0.5
		}
		if dist < 0.5 {
			bDeath.Is = true
			bPos.X = -5000
			bPos.Y = -5000
			assets.Counter += 83
		}
	}

	balls := w.View(component.Pos{}, component.Vel{}, component.Radius{}, component.Slide{}).Filter()
	if AllBallsAreDeath(balls) || inpututil.IsKeyJustPressed(ebiten.KeyBackslash) {
		Regenerate(balls, level)
		ePos.X = math.Cos(mat.RangeF(0, math.Pi*2))*10 + float64(level.Width/2)
		ePos.Y = math.Sin(mat.RangeF(0, math.Pi*2))*10 + float64(level.Height/2) - 2
	}
}

func AllBallsAreDeath(balls []engine.Entity) bool {
	for _, b := range balls {
		var death *component.Death
		b.Get(&death)

		if !death.Is {
			return false
		}
	}

	return true
}

func Regenerate(balls []engine.Entity, level *component.Level) bool {
	for _, b := range balls {
		lw, lh := float64(level.Width), float64(level.Height)
		adjustX, adjustY := lw*0.4, lh*0.4
		rndX := mat.RangeF(adjustX, lw-adjustX)
		rndY := mat.RangeF(adjustY, lh-adjustY)

		var pos *component.Pos
		var death *component.Death
		b.Get(&pos, &death)

		pos.X = rndX
		pos.Y = rndY
		death.Is = false
	}

	return true
}
