package system

import (
	"time"

	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/assets"
)

type Music struct {
	ticker *time.Ticker
}

func NewMusic(replay time.Duration) *Music {
	return &Music{ticker: time.NewTicker(replay)}
}

func (m *Music) Update(w engine.World) {
	select {
	case <-m.ticker.C:
		player := assets.Audio["bg"]
		if !player.IsPlaying() {
			_ = player.Rewind()
			player.Play()
		}
	default:
	}
}
