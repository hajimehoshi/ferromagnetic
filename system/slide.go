package system

import (
	"math"

	"ferromagnetic/helper/vector"

	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/helper/mat"

	"ferromagnetic/component"
)

type Slide struct{}

func NewSlide() *Slide {
	return &Slide{}
}

func (s *Slide) Update(w engine.World) {
	construct, ok := w.View(component.Level{}).Get()
	if !ok {
		return
	}
	var level *component.Level
	construct.Get(&level)

	ball := w.View(component.Pos{}, component.Vel{}, component.Slide{}, component.Gravity{}).Filter()
	tiles := w.View(component.Pos{}, component.Sprite{}).Filter()

	for _, e := range ball {
		var pos *component.Pos
		var vel *component.Vel
		var sliding *component.Slide
		var grav *component.Gravity
		e.Get(&pos, &vel, &sliding, &grav)

		var current component.Pos
		for _, tile := range tiles {
			var p *component.Pos
			tile.Get(&p)
			if int(pos.X) == int(p.X) && int(pos.Y) == int(p.Y) {
				current.X = pos.X
				current.Y = pos.Y

				current.Z = p.Z
				current.OffsetA = p.OffsetA
				current.OffsetB = p.OffsetB
			}
		}

		min := current
		rad := 1
		for j := -rad; j < rad+1; j++ {
			for i := -rad; i < rad+1; i++ {
				// для каждого тайла из области
				mat.Call(tiles, int(pos.X)+i, int(pos.Y)+j, level.Width, level.Height, func(e engine.Entity) {
					var p *component.Pos
					e.Get(&p)

					if p.Z+p.OffsetA+p.OffsetB < min.Z+min.OffsetA+min.OffsetB {
						min.X = p.X
						min.Y = p.Y

						min.X += 0.5
						min.Y += 0.5

						min.Z = p.Z
						min.OffsetA = p.OffsetA
						min.OffsetB = p.OffsetB
					}
				})
			}

		}
		sliding.Target = min

		stepX := sliding.Target.X - pos.X
		stepY := sliding.Target.Y - pos.Y

		speed := (current.Z + current.OffsetA + current.OffsetB) - (sliding.Target.Z + sliding.Target.OffsetA + sliding.Target.OffsetB)
		stepXN, stepYN, _ := vector.Normalize(stepX, stepY, 0)

		vel.L = stepXN * speed
		vel.M = stepYN * speed

		if math.Abs(stepX) < 0.5 {
			vel.L /= 2
		}
		if math.Abs(stepY) < 0.5 {
			vel.M /= 2
		}

		posGravNext := pos.Z - 1 - grav.Value
		if posGravNext < current.Z+current.OffsetA+current.OffsetB {
			vel.N += (current.Z + current.OffsetA + current.OffsetB) - posGravNext
			vel.N /= 4
		}

	}
}
