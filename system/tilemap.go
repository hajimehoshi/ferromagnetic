package system

import (
	"ferromagnetic/helper/geo"
	"ferromagnetic/helper/mat"

	"github.com/hajimehoshi/ebiten/v2"

	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/assets"
	"ferromagnetic/component"
	"ferromagnetic/helper/isometry"
)

type Tilemap struct{}

func NewTilemap() *Tilemap {
	return &Tilemap{}
}

func (t *Tilemap) Update(_ engine.World) {}

func (t *Tilemap) Draw(w engine.World, screen *ebiten.Image) {
	camera, ok := w.View(component.Pos{}, component.Zoom{}).Get()
	if !ok {
		return
	}
	var cameraPos *component.Pos
	var cameraZoom *component.Zoom
	camera.Get(&cameraPos, &cameraZoom)

	offset := isometry.ToScreen(component.NewPos(cameraPos.X*assets.Tilesize, cameraPos.Y*assets.Tilesize))

	w.View(component.Pos{}, component.Sprite{}).Each(func(tile engine.Entity) {
		var tilePos *component.Pos
		var tileSprite *component.Sprite
		tile.Get(&tilePos, &tileSprite)

		op := &ebiten.DrawImageOptions{}
		iso := isometry.ToScreen(component.NewPos(tilePos.X*assets.Tilesize, tilePos.Y*assets.Tilesize))
		op.GeoM.Translate(iso.X, iso.Y-(tilePos.Z+tilePos.OffsetA+tilePos.OffsetB)*assets.TileD)
		op.GeoM.Translate(-offset.X, -offset.Y)
		op.GeoM.Translate(-assets.Tilesize/2, -assets.Tilesize/2)
		op.GeoM.Scale(cameraZoom.Value, cameraZoom.Value)
		op.GeoM.Translate(geo.Image(screen, 0.5))
		op.ColorM.ChangeHSV(0, 1, mat.Normalize(tilePos.Z+tilePos.OffsetA+tilePos.OffsetB, -10, 2, 0, 1))
		screen.DrawImage(tileSprite.Frameset.Image(), op)
	})

}
