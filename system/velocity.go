package system

import (
	"github.com/sedyh/mizu/pkg/engine"

	"ferromagnetic/component"
)

type Velocity struct {
	*component.Pos
	*component.Vel
}

func NewVelocity() *Velocity {
	return &Velocity{}
}

func (v *Velocity) Update(_ engine.World) {
	v.Pos.X += v.Vel.L
	v.Pos.Y += v.Vel.M
	v.Pos.Z += v.Vel.N
}
